<?php

use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'content' => $faker->realText(200),
        'active' => 1
    ];
});
