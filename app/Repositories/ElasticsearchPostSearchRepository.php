<?php

namespace App\Repositories;

use App\Contracts\SearchableContract;
use App\Post;
use App\Traits\NotifiesPostSearches;
use Illuminate\Database\Eloquent\Collection;

class ElasticsearchPostSearchRepository implements SearchableContract
{
    use NotifiesPostSearches {
        sendNewPostSearchNotifications as public sendNewPostSearchNotificationsTrait;
    }

    const LIMIT_SHOW = 200;

    protected $query;

    public function __construct()
    {
        $this->query =  Post::search(null)->take(self::LIMIT_SHOW);
    }

    /**
     * @param null|string $keyword
     * @return SearchableContract
     */
    public function search(?string $keyword = null): SearchableContract
    {
        if ($keyword) {
            $this->query->where('name',$keyword);
        }

        return $this;
    }

    /**
     * @return SearchableContract
     */
    public function active(): SearchableContract
    {
        $this->query->where('active',true);

        return $this;
    }

    /**
     * @return SearchableContract
     */
    public function inactive(): SearchableContract
    {
        $this->query->where('active',false);

        return $this;
    }

    /**
     * @return SearchableContract
     */
    public function alphabetically(): SearchableContract
    {
        $this->query->orderBy('name.keyword', 'asc');
        return $this;
    }

    /**
     * @return SearchableContract
     */
    public function latest(): SearchableContract
    {
        $this->query->orderBy('created_at.keyword', 'desc');

        return $this;
    }

    /**
     * @return Collection
     */
    public function fetch(): Collection
    {
        $result =  $this->query->get();

        $this->sendNewPostSearchNotificationsTrait($result);

        return $result;
    }

    /**
     * @return string
     */
    public function sendNewPostSearchNotifications(): string
    {
        return 'yo';
    }
}